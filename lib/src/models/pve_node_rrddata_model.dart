import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'pve_node_rrddata_model.g.dart';

// iowait: 0.000563001655121094
// netin: 560247.886666667
// netout: 8843.365
// memused: 16582805913.6
// swaptotal: 998240256
// roottotal: 258363686912
// swapused: 63700992
// rootused: 80830384264.5334
// maxcpu: 16
// time: 1586443140
// loadavg: 3.62416666666667
// cpu: 0.355373131264274
// memtotal: 33668456448

abstract class PveNodeRRDDataModel
    implements Built<PveNodeRRDDataModel, PveNodeRRDDataModelBuilder> {
  // Fields
  DateTime? get time;
  double? get iowait;
  double? get netin;
  double? get netout;
  double? get memused;
  double? get memtotal;
  double? get swapused;
  double? get swaptotal;
  double? get roottotal;
  double? get rootused;
  double? get cpu;
  double? get maxcpu;
  double? get loadavg;

  PveNodeRRDDataModel._();

  factory PveNodeRRDDataModel(
          [void Function(PveNodeRRDDataModelBuilder)? updates]) =
      _$PveNodeRRDDataModel;

  static Serializer<PveNodeRRDDataModel> get serializer =>
      _$pveNodeRRDDataModelSerializer;
}
