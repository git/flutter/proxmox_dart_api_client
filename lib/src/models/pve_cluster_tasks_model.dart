import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'pve_cluster_tasks_model.g.dart';

abstract class PveClusterTasksModel
    implements Built<PveClusterTasksModel, PveClusterTasksModelBuilder> {
  static Serializer<PveClusterTasksModel> get serializer =>
      _$pveClusterTasksModelSerializer;

  @BuiltValueField(wireName: 'starttime')
  DateTime get startTime;

  @BuiltValueField(wireName: 'endtime')
  DateTime? get endTime;

  String get status;

  String get type;
  String get upid;
  String get user;
  String get node;

  factory PveClusterTasksModel(
          [void Function(PveClusterTasksModelBuilder)? updates]) =
      _$PveClusterTasksModel;
  PveClusterTasksModel._();
}

class NodeTasksResponse {
  final List<PveClusterTasksModel>? tasks;
  final int total;

  NodeTasksResponse({this.tasks, this.total = 0});
}
